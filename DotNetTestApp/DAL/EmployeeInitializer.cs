﻿using DotNetTestApp.Models;
using System;
using System.Collections.Generic;

namespace DotNetTestApp.DAL
{
    public class EmployeeInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var employees = new List<Employee>
            {
                new Employee{ID=1, Name="John", Position="General Manager", Salary=20000, JoinDate=DateTime.Parse("2005-09-01"), ManagerID=null},
                new Employee{ID=2, Name="Ron", Position="Manager", Salary=15000, JoinDate=DateTime.Parse("2005-09-01"), IsBonusAdded=true, ManagerID=1},
                new Employee{ID=3, Name="Jack", Position="Office Executive", Salary=12000, JoinDate=DateTime.Parse("2005-09-01"), ManagerID=2},
                new Employee{ID=4, Name="Jane", Position="Office Executive", Salary=11000, JoinDate=DateTime.Parse("2005-09-01"), IsBonusAdded=true, ManagerID=2},
                new Employee{ID=5, Name="Hun", Position="Office Executive", Salary=10000, JoinDate=DateTime.Parse("2005-09-01"), IsBonusAdded=true, ManagerID=2},
                new Employee{ID=6, Name="Tod", Position="Office Executive", Salary=11500, JoinDate=DateTime.Parse("2005-09-01"), IsBonusAdded=true, ManagerID=2}
            };

            employees.ForEach(emp => context.Employees.Add(emp));
            context.SaveChanges();
        }
    }
}