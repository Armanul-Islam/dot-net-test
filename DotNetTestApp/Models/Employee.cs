﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DotNetTestApp.Models
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public decimal Salary { get; set; }

        [Display(Name = "Join Date")]
        public DateTime JoinDate { get; set; }

        [Display(Name = "Is Bonus Added")]
        public bool IsBonusAdded { get; set; }

        public int? ManagerID { get; set; }
        public Employee Manager { get; set; }
    }
}