﻿using DotNetTestApp.DAL;
using DotNetTestApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DotNetTestApp.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(int? employeeID)
        {
            if (employeeID == null)
            {
                return View(new List<EmployeeVM>());
            }

            Employee employee = db.Employees.FirstOrDefault(e => e.ID == employeeID);
            if (employee == null)
            {
                return HttpNotFound();
            }

            while (employee.ManagerID != null)
            {
                employee = db.Employees.FirstOrDefault(e => e.ID == employee.ManagerID);
            }

            IList<EmployeeVM> employeesWithSalaryAndBonus = new List<EmployeeVM>
            {
                GetEmployeeVMWithSalaryAndBonus(employee,employee)
            };

            IList<Employee> subordinates = GetSubordinatesOfManager(employee);
            foreach (Employee subordinate in subordinates)
            {
                employeesWithSalaryAndBonus.Add(GetEmployeeVMWithSalaryAndBonus(subordinate, employee));
            }

            ViewData["EmployeeID"] = employeeID;
            return View(employeesWithSalaryAndBonus);
        }

        private IList<Employee> GetSubordinatesOfManager(Employee manager)
        {
            var subordinates = db.Employees
                .Where(e => e.ManagerID == manager.ID)
                .ToList();

            List<Employee> result = new List<Employee>();
            foreach (var subordinate in subordinates)
            {
                if (subordinate.IsBonusAdded)
                {
                    result.Add(subordinate);
                }

                result.AddRange(GetSubordinatesOfManager(subordinate));
            }

            return result;
        }

        private EmployeeVM GetEmployeeVMWithSalaryAndBonus(Employee employee, Employee rootManager)
        {
            EmployeeVM employeeVM = new EmployeeVM()
            {
                ID = employee.ID,
                Name = employee.Name,
                Position = employee.Position,
                SalaryWithBonus = employee.Salary + GetBonus(employee, rootManager),
                JoinDate = employee.JoinDate
            };

            return employeeVM;
        }

        private decimal GetBonus(Employee employee, Employee rootManager)
        {
            Employee immediateManager;
            if (employee.Equals(rootManager))
            {
                immediateManager = rootManager;
            }
            else
            {
                immediateManager = db.Employees.FirstOrDefault(e => e.ID == employee.ManagerID);
            }

            decimal bonusAmount = 0;
            if (IsRootManagerFourYears(rootManager.JoinDate) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                bonusAmount += 10000;

                if (employee.JoinDate.Date < immediateManager.JoinDate.Date)
                {
                    bonusAmount += 2000;
                }
            }
            else if (IsRootManagerFourYears(rootManager.JoinDate) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                bonusAmount += 8000;

                if (employee.JoinDate.Date >= immediateManager.JoinDate.Date)
                {
                    bonusAmount += 1000;
                }
            }
            else if (!IsRootManagerFourYears(rootManager.JoinDate) && DateTime.IsLeapYear(DateTime.Now.Year))
            {
                bonusAmount += 5000;

                if (employee.JoinDate.Date < immediateManager.JoinDate.Date)
                {
                    bonusAmount += 1000;
                }
            }
            else if (!IsRootManagerFourYears(rootManager.JoinDate) && !DateTime.IsLeapYear(DateTime.Now.Year))
            {
                bonusAmount += 3000;

                if (employee.JoinDate.Date >= immediateManager.JoinDate.Date)
                {
                    bonusAmount += 500;
                }
            }

            return bonusAmount;
        }

        private bool IsRootManagerFourYears(DateTime joiningDate)
        {
            return DateTime.Now.AddYears(-4).Date >= joiningDate.Date;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}