﻿namespace DotNetTestApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Position = c.String(),
                        Salary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        JoinDate = c.DateTime(nullable: false),
                        IsBonusAdded = c.Boolean(nullable: false),
                        ManagerID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employees", t => t.ManagerID)
                .Index(t => t.ManagerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "ManagerID", "dbo.Employees");
            DropIndex("dbo.Employees", new[] { "ManagerID" });
            DropTable("dbo.Employees");
        }
    }
}
