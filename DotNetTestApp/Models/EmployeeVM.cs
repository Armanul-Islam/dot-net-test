﻿using System;
namespace DotNetTestApp.Models
{
    public class EmployeeVM
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public decimal SalaryWithBonus { get; set; }

        public DateTime JoinDate { get; set; }
    }
}